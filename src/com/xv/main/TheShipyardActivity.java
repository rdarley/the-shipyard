package com.xv.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TheShipyardActivity extends Activity {
    /** Called when the activity is first created. */
    private ListView dockList;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dockList = (ListView) findViewById(R.id.dockList);
        dockList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1 , docks));
        dockList.setTextFilterEnabled(true);
        dockList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				Toast.makeText(getApplicationContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
			}
        	
        });
    }
	
	static final String[] docks = new String[] {
		"Battlestar Galactica", "Star Trek", "Star Wars"};
}